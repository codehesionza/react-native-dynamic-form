# React Native Dynamic Form

> ${DESCRIPTION}

[![NPM Version][npm-image]][npm-url]

## Install
First, add the following to your `.npmrc` file to allow npm to use the Codehesion package repo (more info on the `.npmrc` file can be found [here](https://docs.npmjs.com/files/npmrc)):
```
@codehesionza:registry=https://gitlab.com/api/v4/packages/npm/
```

Then, run the following command:
```bash
npm i -S @codehesionza/react-native-dynamic-form
```

## Basic Usage
### Model Example
```javascript
import {
    DatePickerRenderer,
    DefaultRenderer,
    Model,
    PickerRenderer, 
    NumberRenderer
} from '@codehesionza/react-native-dynamic-form';

export const model = new Model({
    fields: [
        {
            name: 'country-id',
            label: 'Country',
            value: '',
            placeholder: 'Select your country',
            render: PickerRenderer,
            items: [{ name: 'South Africa', id: 'South Africa' }],
            itemData: {
                idField: 'id',
                nameField: 'name'
            },
            dependent: {
                enabled: true,
                name: 'payout-method-id'
            },
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a description'
                }
            }
        },
        {
            name: 'description',
            label: 'Description',
            render: DefaultRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a description'
                }
            }
        },
        {
            name: 'number',
            label: 'Number',
            render: NumberRenderer,
            extra: {
                max: 10,
                min: 0
            },
            hide: true,
            defaultValue: 100,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a number'
                }
            }
        },
        {
            name: 'start_date',
            label: 'Start Date',
            defaultValue: moment().format('YYYY-MM-DD'),
            render: DatePickerRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a start date'
                }
            },
            filter: {
                type: 'date',
                defaultValue: moment().format('YYYY-MM-DD')
            }
        },
        {
            name: 'end_date',
            label: 'End Date',
            defaultValue: moment().format('YYYY-MM-DD HH:mm'),
            render: DatePickerRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter an end date'
                }
            },
            filter: {
                type: 'date',
                defaultValue: moment().format('YYYY-MM-DD')
            }
        },
        {
            name: 'start_time',
            label: 'Start Time',
            defaultValue: moment().format('HH:mm'),
            render: DatePickerRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a start time'
                }
            },
            filter: {
                type: 'time',
                defaultValue: moment().format('HH:mm')
            }
        }
    ]
});
```

### Reducer Example
```javascript
import { model as exampleModel } from './example.model';

const initialState = {
    country: '',
    description: '',
    number: 0,
    start_date: '',
    end_date: '',
    start_time: '00:00',
    interest: ''
};

exampleModel.setInitialState(initialState);

exampleModel.addActionStates([{ name: 'RESET', operation: () => exampleModel.getInitialState() }]);

export const setCountryAction = exampleModel.getActionFromFieldName('country');

export const setDescriptionAction = exampleModel.getActionFromFieldName('description');

export const setNumberAction = exampleModel.getActionFromFieldName('number');

export const setStartDateAction = exampleModel.getActionFromFieldName('start_date');

export const setEndDateAction = exampleModel.getActionFromFieldName('end_date');

export const setStartTimeAction = exampleModel.getActionFromFieldName('start_time');

export const setInterestAction = exampleModel.getActionFromFieldName('interest');

export const resetAction = exampleModel.getAction('RESET');

export default exampleModel.getReducer();

```
### Component Example
```jsx harmony
import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import { boundMethod } from 'autobind-decorator';
import { model as exampleModel } from '../../reducers/example-reducer/example.model';
import DynamicForm from '../../components/form/dynamic-form.component';
import theme from '../../theme/theme';

export default class ExampleScene extends Component {
    render() {
        const { isLoading } = this.props;
        return (
            <View>
                <DynamicForm
                    fields={exampleModel.fields}
                    theme={{theme}}
                    sceneKey={ExampleScene.key}
                    submit={{ label: 'Sign In', onAction: this.onSubmit }}
                    isSubmitting={isLoading}
                />
            </View>
        );
    }

    @boundMethod
    onSubmit(data) {
        const { onSubmit } = this.props;

        if (onSubmit) {
            onSubmit(data).then(() => {

            });
        }
    }
}

ExampleScene.key = 'exampleScene';

ExampleScene.propTypes = {
    isLoading: PropTypes.bool,

    onSubmit: PropTypes.func
};
```

[npm-image]: https://img.shields.io/npm/v/live-xxx.svg
[npm-url]: https://gitlab.com/api/v4/packages/npm/react-native-dynamic-form
