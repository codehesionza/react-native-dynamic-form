export default {
    white: '#fff',
    placeholder: 'rgba(255,255,255, 0.5)',
    required: '#CB0805',
    success: '#107e05',
    error: '#CB0805',
    backgroundDarkGrey: '#959ba3',
    black30: 'rgba(255,255,255,0.3)',
    transparent: 'rgba(255,255,255, 0)',
    darkGreyText: '#343F4B'
};
