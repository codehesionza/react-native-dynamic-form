export default {
    Input: {
        containerStyle: {
            margin: 10,
            marginLeft: 10,
            marginRight: 10
        },
        inputStyle: {
            paddingLeft: 10
        },
        inputContainerStyle: {
            shadowColor: '#000',
            shadowOffset: {
                width: 0,
                height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,

            marginLeft: 0,
            marginRight: 10
        },
        leftIconContainerStyle: {
            paddingLeft: 0
        },
        rightIconContainerStyle: {
            paddingRight: 10
        }
    }
};
