import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import { boundMethod } from 'autobind-decorator';
import { model as exampleModel } from '../../reducers/example-reducer/example.model';
import DynamicForm from '../../components/form/dynamic-form.component';
import theme from '../../theme/theme';

export default class ExampleScene extends Component {
    render() {
        const { isLoading } = this.props;
        return (
            <View>
                <DynamicForm
                    fields={exampleModel.fields}
                    theme={{ theme }}
                    sceneKey={ExampleScene.key}
                    submit={{ label: 'Sign In', onAction: this.onSubmit }}
                    isSubmitting={isLoading}
                />
            </View>
        );
    }

    @boundMethod
    onSubmit(data) {
        const { onSubmit } = this.props;

        if (onSubmit) {
            onSubmit(data);
        }
    }
}

ExampleScene.key = 'exampleScene';

ExampleScene.propTypes = {
    isLoading: PropTypes.bool,

    onSubmit: PropTypes.func
};
