import PropTypes from 'prop-types';

export const Field = {
    name: '',
    label: '',
    placeholder: '',
    value: '',
    type: undefined,
    itemData: {
        idField: 'id',
        nameField: 'name'
    },
    dependent: {
        enabled: false,
        name: ''
    },
    validation: {
        presence: {
            allowEmpty: false,
            message: '^Please enter a description'
        }
    },
    render: null,
    onChange: (name, value) => {
        console.warn(name, value);
    },
    source: () => {
        console.warn('Here');
    },
    get: (value) => {
        console.warn(value);
    },
    customStyles: {}
};

Field.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.any,
    type: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    itemData: PropTypes.shape({
        idField: PropTypes.string,
        nameField: PropTypes.string
    }),
    dependent: PropTypes.shape({
        enabled: PropTypes.bool,
        name: PropTypes.string
    }),
    validation: PropTypes.shape({
        presence: PropTypes.shape({
            allowEmpty: PropTypes.bool,
            message: PropTypes.string
        })
    }),

    render: PropTypes.func,
    onChange: PropTypes.func,
    source: PropTypes.func,
    get: PropTypes.func
};
