import _ from 'lodash';
import { createAction } from '../helpers/actions.helper';

export class ReducerFactory {
    constructor(name, fields = [], actionStates = [], initialState = {}) {
        this.name = name;
        this.fields = fields;
        this.actionStates = actionStates;
        this.initialState = initialState;
        this.actions = [];

        this.setActionStates();
        this.setActions();
    }

    addActionStates(states) {
        states.forEach((state) => {
            this.actionStates.push(state);
        });
        this.setActions();
    }

    setActionStates() {
        this.actionStates = _.reduce(
            this.fields,
            (result, field) => {
                result.push({
                    name: `SET_${_.get(field, 'name', '')
                        .toString()
                        .toUpperCase()}`
                });

                return result;
            },
            this.actionStates
        );
    }

    getInitialState() {
        return this.fields.reduce((state, current) => {
            let value = _.get(current, 'defaultValue', '');
            if (_.has(this.initialState, current.name)) {
                value = _.get(this.initialState, current.name, value);
            }

            state[current.name] = value;

            return state;
        }, {});
    }

    getInstance() {
        const initialState = this.getInitialState();
        return (state = initialState, action) => {
            for (let i = 0; i < this.actionStates.length; i++) {
                const current = this.actionStates[i];
                if (action.type === this.getNamespacedActionType(_.get(current, 'name'))) {
                    if (_.has(current, 'operation')) {
                        return current.operation(state, action);
                    }

                    const name = this.getFieldName(action.type);
                    return { ...state, [name]: action.payload };
                }
            }

            return state;
        };
    }

    setActions() {
        this.actions = this.actionStates.map((actionState) => {
            return this.createAction(_.get(actionState, 'name'));
        });
    }

    /**
     *
     * @param {string} actionName
     * @returns {{actionType, action}}
     */
    createAction(actionName) {
        return createAction(this.name, actionName);
    }

    /**
     *
     * @param {string} fieldName
     * @returns {function | null}
     */
    getActionFromFieldName(fieldName) {
        const actionName = `SET_${fieldName.toUpperCase()}`;
        return this.getAction(actionName);
    }

    /**
     *
     * @param actionName
     * @returns {null|*}
     */
    getAction(actionName) {
        const action = this.actions.find((action) => {
            return action.actionType === this.getNamespacedActionType(actionName);
        });

        if (action) {
            return action.action;
        }

        return null;
    }

    getFieldName(actionType) {
        return actionType
            .toString()
            .replace(`${this.name}/`, '')
            .replace('SET_', '')
            .toLowerCase();
    }

    getNamespacedActionType(actionName) {
        return `${this.name}/${actionName}`;
    }

    getActionName(fieldName, prefix = 'set') {
        return `${prefix}${_.upperFirst(fieldName)}`;
    }
}
