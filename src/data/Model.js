import _ from 'lodash';

import {
    getExtraValidationParams,
    validateField,
    validateFields
} from '../helpers/validate.helper';
import { ReducerFactory } from '../factories/reducer-factory';

/**
 * @typedef {Object} Validation
 */

/**
 * @typedef {Object} Field
 * @property {string} name - The unique field name
 * @property {string} label - The display label for the field
 * @property {string} placeholder - The placeholder/prompt text that is displayed
 * @property {string|func} type - The type of field
 * @property {Array} items - The items required by a picker or list component
 * @property {Validation} validation
 */

/**
 * Class representing a Model.
 */
export class Model {
    /**
     * Creates an instance of this class
     *
     * @param props
     * @param {Field[]} fields - The list of fields that represent this model
     */
    constructor({ name, fields = [], actions = [], initialState = {} }) {
        this.fields = this.getFields(fields);

        this.reducerFactory = new ReducerFactory(name, fields, actions, initialState);
    }

    /**
     *
     * @param {Object} state
     * @param {Object} actionMap
     */
    setFromState(state, actionMap = []) {
        this.fields = this.fields.map((field) => {
            if (_.has(actionMap, this.reducerFactory.getActionName(field.name))) {
                field.action = _.get(actionMap, this.reducerFactory.getActionName(field.name));
            }

            if (_.has(state, field.name)) {
                field.value = _.get(state, field.name);
            }

            return field;
        });
    }

    /**
     *
     * @returns {Function}
     */
    getReducer() {
        return this.reducerFactory.getInstance();
    }

    /**
     *
     * @param {string} name
     * @returns {Object}
     */
    getAction(name) {
        return this.reducerFactory.getAction(name);
    }

    /**
     *
     * @param {string} name
     * @returns {*}
     */
    getActionFromFieldName(name) {
        return this.reducerFactory.getActionFromFieldName(name);
    }

    /**
     *
     * @param {Array} states
     */
    addActionStates(states) {
        this.reducerFactory.addActionStates(states);
    }

    /**
     * Sets the initial state of the reducer.
     */
    setInitialState(initialState = {}) {
        this.reducerFactory.initialState = initialState;
    }

    /**
     * Returns the initial state of the reducer
     * (which is dynamically populated from the field definitions).
     *
     * @returns {Object}
     */
    getInitialState() {
        return this.reducerFactory.getInitialState();
    }

    /**
     * The function that processes and returns the required array of fields.
     * Any fields that are excluded or aren't editable are not included in the returned array.
     *
     * @param {Field[]} fields
     * @returns {Array}
     */
    getFields(fields = []) {
        return this.cloneFields(fields).reduce((result, field) => {
            if (this.isEditable(field)) {
                field.show = true;
                const currentField = this.get(field.name);
                field.value =
                    currentField && currentField.value
                        ? currentField.value
                        : field.defaultValue || '';
                field.required =
                    field.validation &&
                    field.validation.presence &&
                    field.validation.presence.allowEmpty !== undefined
                        ? !field.validation.presence.allowEmpty
                        : false;
                result.push(field);
            }

            return result;
        }, []);
    }

    /**
     * Finds all the fields by their names
     *
     * @param {string[]} names
     * @returns {Field[]}
     */
    findFieldsByName(names = []) {
        return this.fields.reduce((final, field) => {
            if (names.includes(field.name)) {
                final.push(field);
            }

            return final;
        }, []);
    }

    /**
     * Sets a value in a field instance (found by the name given).
     *
     * @param {string} name
     * @param {any} value
     */
    set(name, value) {
        this.fields = _.map(this.fields, (field) => {
            if (field.name === name) {
                const computedValue = this.getValue(field, value);
                field.value = computedValue;
                if (_.has(field, 'action') && typeof field.action === 'function') {
                    field.action(computedValue);
                }
            }

            return field;
        });
    }

    getValue(field, value) {
        let computedValue = value;
        if (_.has(field, 'modifier') && typeof field.modifier === 'function') {
            computedValue = field.modifier(value);
        }

        return computedValue;
    }

    /**
     * Get a field object from the fields array.
     *
     * @param name
     * @returns {Object}
     */
    get(name) {
        return _.find(this.fields, (field) => field.name === name);
    }

    /**
     * Checks whether or not this field can be edited on the form.
     *
     * @param {Object} field
     * @returns {boolean}
     */
    isEditable(field) {
        return field.editable === undefined || field.editable !== false;
    }

    /**
     * Retrieves the label from a field object, or dynamically
     * generates it based on the name if the label field does not exist.
     *
     * @param {Object} field
     * @returns {string}
     */
    getLabel(field) {
        if (field.label === undefined) {
            const header = field.name.charAt(0).toUpperCase() + field.name.slice(1);
            return header.replace(/_/g, ' ');
        }

        return field.label;
    }

    /**
     * Validates all fields in the form and sets any errors that are returned.
     */
    validate() {
        const errors = validateFields(this.fields);
        this.setErrors(errors);
    }

    /**
     * Validates single field in the form and sets any errors that are returned.
     */
    validateField(name, value) {
        const validationParams = _.merge({ [name]: value }, getExtraValidationParams(this.fields));
        const error = validateField(name, validationParams, _.get(name, 'validation'));
        this.setErrors({ [name]: error });
    }

    /**
     * Sets the errors contained in the errors object parameter.
     *
     * @param {Object} errors
     */
    setErrors(errors = undefined) {
        if (errors) {
            this.fields.map((field) => {
                if (_.has(errors, field.name)) {
                    field.error = _.nth(_.get(errors, field.name), 0);
                } else {
                    field.error = undefined;
                }

                return field;
            });
        } else {
            this.clearErrors();
        }
    }

    /**
     * Clears all errors in the fields
     */
    clearErrors() {
        this.fields.map((field) => {
            field.error = undefined;

            return field;
        });
    }

    /**
     * Checks if this model contains any errors
     *
     * @returns {boolean}
     */
    hasErrors() {
        let errorCount = 0;
        this.fields.forEach((field) => {
            if (field.error !== undefined) {
                errorCount++;
            }
        });

        return errorCount > 0;
    }

    /**
     * Takes in a flat data object and generates a multi-level object.
     * This functions turns a field like `{profile.first_name: 'Name'}` into
     * ```
     * {
     *   profile: {
     *     first_name: 'Name'
     *   }
     * }
     * ```
     *
     * @param {Object} data
     * @returns {Object}
     */
    processData(data) {
        const processed = data;
        Object.keys(data).forEach((name) => {
            if (name.includes('.')) {
                let ref = processed;
                const fields = name.split('.');
                fields.forEach((field, index) => {
                    let value = undefined;
                    if (index < fields.length - 1) {
                        value = ref[field] || {};
                    } else {
                        value = data[name];
                    }
                    ref[field] = value;
                    ref = ref[field];
                });

                delete processed[name];
            }
        });

        return processed;
    }

    /**
     * Clones a fields array so references are not preserved
     * (this prevents data problems across different views/forms)
     *
     * @param {Array} fields
     * @returns {Array}
     *
     */
    cloneFields(fields = []) {
        return fields.map((field) => Object.assign(field));
    }

    /**
     * Sets, processes and returns the final data object for submission
     * (set from the values contained within the field objects).
     *
     * @returns {Object}
     */
    getReturnObject() {
        const data = {};
        this.fields.forEach((field) => {
            data[field.name] = field.value;
        });

        return this.processData(data);
    }

    /**
     * Set the values in all the field objects to their defaults.
     *
     * @returns {Object}
     */
    clear() {
        const initialState = this.getInitialState();
        this.fields = this.fields.map((field) => {
            if (_.has(initialState, field.name)) {
                field.value = _.get(initialState, field.name, field.defaultValue);
            }

            return field;
        });
    }
}
