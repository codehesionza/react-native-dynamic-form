import { model as exampleModel } from './example.model';

const initialState = {
    country: '',
    description: '',
    number: 0,
    start_date: '',
    end_date: '',
    start_time: '00:00',
    interest: ''
};

exampleModel.setInitialState(initialState);

exampleModel.addActionStates([{ name: 'RESET', operation: () => exampleModel.getInitialState() }]);

export const setCountryAction = exampleModel.getActionFromFieldName('country');

export const setDescriptionAction = exampleModel.getActionFromFieldName('description');

export const setNumberAction = exampleModel.getActionFromFieldName('number');

export const setStartDateAction = exampleModel.getActionFromFieldName('start_date');

export const setEndDateAction = exampleModel.getActionFromFieldName('end_date');

export const setStartTimeAction = exampleModel.getActionFromFieldName('start_time');

export const setInterestAction = exampleModel.getActionFromFieldName('interest');

export const resetAction = exampleModel.getAction('RESET');

export default exampleModel.getReducer();
