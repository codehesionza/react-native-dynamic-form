import moment from 'moment';

import { Model } from '../../data/Model';
import { PickerRenderer } from '../../renderers/picker.renderer';
import { DefaultRenderer } from '../../renderers/default.renderer';
import { NumberRenderer } from '../../renderers/number.renderer';
import { DatePickerRenderer } from '../../renderers/date-picker.renderer';

export const model = new Model({
    name: 'example',
    fields: [
        {
            name: 'country',
            label: 'Country',
            value: '',
            placeholder: 'Select your country',
            render: PickerRenderer,
            items: [{ name: 'South Africa', id: 'South Africa' }],
            itemData: {
                idField: 'id',
                nameField: 'name'
            },
            dependent: {
                enabled: true,
                name: 'payout-method-id'
            },
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a description'
                }
            }
        },
        {
            name: 'description',
            label: 'Description',
            render: DefaultRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a description'
                }
            }
        },
        {
            name: 'number',
            label: 'Number',
            render: NumberRenderer,
            extra: {
                max: 10,
                min: 0
            },
            hide: true,
            defaultValue: 100,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a number'
                }
            }
        },
        {
            name: 'start_date',
            label: 'Start Date',
            defaultValue: moment().format('YYYY-MM-DD'),
            render: DatePickerRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a start date'
                }
            },
            filter: {
                type: 'date',
                defaultValue: moment().format('YYYY-MM-DD')
            }
        },
        {
            name: 'end_date',
            label: 'End Date',
            defaultValue: moment().format('YYYY-MM-DD HH:mm'),
            render: DatePickerRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter an end date'
                }
            },
            filter: {
                type: 'date',
                defaultValue: moment().format('YYYY-MM-DD')
            }
        },
        {
            name: 'start_time',
            label: 'Start Time',
            defaultValue: moment().format('HH:mm'),
            render: DatePickerRenderer,
            validation: {
                presence: {
                    allowEmpty: false,
                    message: '^Please enter a start time'
                }
            },
            filter: {
                type: 'time',
                defaultValue: moment().format('HH:mm')
            }
        },
        {
            name: 'interest',
            label: 'Interest',
            render: PickerRenderer,
            itemData: {
                idField: 'value',
                nameField: 'label'
            },
            items: [
                { value: 'Pottery', label: 'Pottery' },
                { value: 'Knitting', label: 'Knitting' },
                { value: 'Running', label: 'Running' }
            ]
        }
    ]
});

export const exampleItems = [
    {
        id: 1,
        name: 'Example',
        description: 'Example',
        start_date: moment().format('YYYY-MM-DD'),
        end_date: moment().format('YYYY-MM-DD HH:mm'),
        start_time: moment().format('HH:mm'),
        interests: ['Pottery', 'Running'],
        second_interests: 'Pottery',
        user: {
            profile: {
                first_name: 'First Name',
                last_name: 'Last Name',
                mobile_number: '0111234567'
            },
            image: {
                src: '#'
            }
        }
    }
];
