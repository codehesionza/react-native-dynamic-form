import { ReducerFactory } from '../../factories/reducer-factory';

const fields = [
    {
        name: 'models',
        defaultValue: []
    }
];

const initialState = {
    models: []
};

const reducerFactory = new ReducerFactory('account', fields, [], initialState);

export const setModelsAction = reducerFactory.getActionFromFieldName('models');

reducerFactory.addActionStates([
    { name: 'RESET', operation: () => reducerFactory.getInitialState() }
]);

export default reducerFactory.getInstance();
