import _ from 'lodash';
import { setModelsAction } from './model.reducer';

export const setModel = (model) => {
    return (dispatch, getState) => {
        const { models = [] } = getState().modelReducer;

        const existingModel = models.find(
            (current) => _.get(model, 'name') === _.get(current, name)
        );

        if (existingModel && _.has(model, 'merge')) {
            _.get(model, 'merge')(model);
        } else {
            models.push(model);
        }

        dispatch(setModelsAction(models));
    };
};
