import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    FlatList,
    Modal,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    ViewPropTypes
} from 'react-native';
import { Icon, SearchBar, withTheme, Button } from 'react-native-elements';
import { boundMethod } from 'autobind-decorator';
import _ from 'lodash';

import Error from '../utility/error.component';
import colors from '../../theme/colors';
import LoadingIndicator from '../utility/loading-indicator.component';
import Label from '../form/label.component';
import { getItemsForPicker, onDependentValueUpdate } from '../../helpers/list.helper';

class Picker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            items: null,
            searchTerm: ''
        };

        this.debouncedSearch = _.debounce(this.search, 1000);
    }

    componentDidMount() {
        this.updateItems();
    }

    componentDidUpdate(prevProps) {
        if (!_.isEqual(prevProps.field, this.props.field)) {
            this.updateItems();
        }
    }

    render() {
        const { field, theme, containerStyle, inputContainerStyle, labelStyle } = this.props;
        const { modalVisible } = this.state;
        const icon = _.get(field, 'icon.name', 'caret-down');

        const disabled = !_.get(field, 'enabled', false);
        const showIcon = _.get(field, 'icon.enabled', true);

        let disabledStyle = {};
        if (disabled) {
            disabledStyle = { opacity: 0.25 };
        }

        return (
            <View
                style={[
                    styles.containerStyle,
                    _.get(theme, 'Input.containerStyle'),
                    containerStyle,
                    disabledStyle
                ]}
            >
                <Label
                    required={_.get(field, 'required', false)}
                    label={_.get(field, 'label')}
                    labelStyle={labelStyle}
                />
                <TouchableOpacity
                    disabled={disabled}
                    onPress={this.showModal}
                    style={[
                        styles.inputContainerStyle,
                        _.get(theme, 'Input.inputContainerStyle'),
                        inputContainerStyle
                    ]}
                >
                    <View style={styles.touchableOpacity}>
                        {this.renderSelectedValue()}
                        {showIcon && <Icon name={icon} style={styles.dropdownIcon} />}
                    </View>
                </TouchableOpacity>
                <Error error={_.get(field, 'error')} />

                <Modal
                    visible={modalVisible}
                    transparent
                    animationType="fade"
                    onRequestClose={this.hideModal}
                >
                    <TouchableWithoutFeedback onPress={this.hideModal}>
                        {this.renderModal()}
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        );
    }

    renderModal() {
        const { renderModal, theme, modalContainerStyle, modalStyle } = this.props;

        const header = this.renderHeader();
        const searchBar = this.renderSearchBar();
        const list = this.renderList();
        const addItemButton = this.renderAddItemButton();

        if (renderModal) {
            return renderModal({ header, searchBar, list });
        }

        return (
            <View
                style={[
                    styles.modalContainerStyle,
                    _.get(theme, 'Modal.containerStyle'),
                    modalContainerStyle
                ]}
            >
                <View style={[styles.modalStyle, _.get(theme, 'Modal.modalStyle'), modalStyle]}>
                    {header}
                    {searchBar}
                    {addItemButton}
                    {list}
                </View>
            </View>
        );
    }

    renderSelectedValue() {
        const { field, placeholderTextStyle, inputTextStyle, theme } = this.props;

        const selectedValue = _.get(field, 'value');

        if (!selectedValue) {
            return (
                <Text
                    style={[
                        styles.inputTextStyle,
                        styles.placeholderTextStyle,
                        _.get(theme, 'Input.placeholderTextStyle'),
                        placeholderTextStyle
                    ]}
                >
                    {_.get(field, 'placeholder')}
                </Text>
            );
        }
        const selectedItem = _.find(
            getItemsForPicker(field),
            (item) => _.get(item, 'value') === selectedValue
        );
        const selectedLabel = _.get(selectedItem, 'label');

        return <Text style={[styles.inputTextStyle, inputTextStyle]}>{selectedLabel}</Text>;
    }

    renderHeader() {
        const {
            theme,
            pickerTitleContainerStyle,
            closeButtonContainerStyle,
            closeButtonTextStyle
        } = this.props;
        return (
            <View
                style={[
                    styles.pickerTitleContainerStyle,
                    _.get(theme, 'PickerInput.pickerTitleContainerStyle'),
                    pickerTitleContainerStyle
                ]}
            >
                <TouchableOpacity
                    onPress={this.hideModal}
                    style={[
                        styles.closeButtonContainerStyle,
                        _.get(theme, 'Modal.closeButtonContainerStyle'),
                        closeButtonContainerStyle
                    ]}
                >
                    <Text
                        style={[
                            styles.closeButtonTextStyle,
                            _.get(theme, 'Modal.closeButtonTextStyle'),
                            closeButtonTextStyle
                        ]}
                    >
                        Close
                    </Text>
                </TouchableOpacity>
                {this.renderPickerTitle()}
            </View>
        );
    }

    renderPickerTitle() {
        const { field, theme, titleTextStyle } = this.props;

        const title = _.get(field, 'placeholder', '');

        if (!title) {
            return null;
        }

        return (
            <Text
                style={[
                    styles.pickerTitleTextStyle,
                    _.get(theme, 'PickerInput.titleTextStyle'),
                    titleTextStyle
                ]}
            >
                {title}
            </Text>
        );
    }

    renderSearchBar() {
        const { searchPlaceholder = 'Search', searchable = true } = this.props;
        const { searchTerm } = this.state;

        if (!searchable) {
            return null;
        }

        return (
            <SearchBar
                placeholder={searchPlaceholder}
                value={searchTerm}
                onChangeText={this.onSearch}
                onClear={this.onClear}
                searchBarContainerStyle={styles.searchBar}
            />
        );
    }

    renderList() {
        const { items, searchTerm } = this.state;
        if (_.isEmpty(items)) {
            if (searchTerm) {
                return <Text style={styles.noResultsText}>No results found.</Text>;
            }
            if (!_.isNull(items)) {
                return <Text style={styles.noResultsText}>No Options Available</Text>;
            }

            return (
                <LoadingIndicator
                    loading
                    color={colors.backgroundDarkGrey}
                    containerStyle={styles.spinnerView}
                />
            );
        }

        return (
            <FlatList
                style={styles.flatListStyle}
                keyExtractor={this.keyExtractor}
                showsVerticalScrollIndicator={false}
                overScrollMode="never"
                keyboardShouldPersistTaps="always"
                numColumns={1}
                data={items}
                renderItem={this.renderListItem}
            />
        );
    }

    renderAddItemButton() {
        const {
            field,
            addItemButtonContainerStyle,
            addItemButtonStyle,
            addItemButtonTitleStyle
        } = this.props;

        const onAddItem = _.get(field, 'onAddItem');
        const defaultAddItemMessage = `Haven't added a ${_.get(field, 'label')} yet? Add one now.`;
        const addItemMessage = _.get(field, 'addItemMessage', defaultAddItemMessage);

        if (onAddItem) {
            return (
                <Button
                    containerStyle={[
                        styles.addItemButtonContainerStyle,
                        addItemButtonContainerStyle
                    ]}
                    buttonStyle={[styles.addItemButtonStyle, addItemButtonStyle]}
                    titleStyle={[styles.addItemButtonTitleStyle, addItemButtonTitleStyle]}
                    onPress={this.onAddItem}
                    title={addItemMessage}
                />
            );
        }
    }

    @boundMethod
    renderListItem({ item }) {
        const { field, theme, selectedIconStyle } = this.props;

        const selectedValue = _.get(field, 'value');
        const selectedIcon = _.get(field, 'selectedIcon', { name: 'check' });
        const label = _.get(item, 'label');
        const value = _.get(item, 'value');

        return (
            <TouchableOpacity
                style={styles.listRowClickTouchStyle}
                onPress={this.onListItemPress(value)}
            >
                <View style={styles.listRowContainerStyle}>
                    <Text style={styles.listTextViewStyle}>{label}</Text>
                    {selectedValue === value ? (
                        <Icon
                            {...selectedIcon}
                            style={[
                                styles.selectedIconStyle,
                                _.get(theme, 'PickerInput.selectedIconStyle'),
                                selectedIconStyle
                            ]}
                        />
                    ) : null}
                </View>
            </TouchableOpacity>
        );
    }

    @boundMethod
    onListItemPress(value) {
        return () => {
            this.onValueChange(value);
        };
    }

    @boundMethod
    keyExtractor(item) {
        return _.get(item, 'key', '');
    }

    @boundMethod
    showModal() {
        this.setState({ modalVisible: true, searchTerm: '' });
        this.updateItems();
    }

    @boundMethod
    hideModal() {
        this.setState({ modalVisible: false });
    }

    @boundMethod
    onSearch(searchTerm) {
        this.setState(() => ({ searchTerm }));

        this.debouncedSearch();
    }

    @boundMethod
    onClear() {
        const { field } = this.props;
        this.setState(() => ({ searchTerm: undefined, items: getItemsForPicker(field) }));
    }

    @boundMethod
    onAddItem() {
        const { field } = this.props;

        const onAddItem = _.get(field, 'onAddItem');

        if (onAddItem) {
            this.hideModal();
            onAddItem();
        }
    }

    @boundMethod
    search() {
        const { field } = this.props;

        this.setState((state) => {
            const searchTerm = _.get(state, 'searchTerm');
            if (searchTerm) {
                const filteredItems = _.filter(_.get(field, 'items'), (item) =>
                    _.includes(
                        _.lowerCase(_.get(item, 'label')),
                        _.lowerCase(_.get(state, 'searchTerm'))
                    )
                );
                return { items: filteredItems };
            }

            return { items: getItemsForPicker(field) };
        });
    }

    @boundMethod
    onValueChange(value, autoSet) {
        const { field, onChange, getFieldAttribute } = this.props;

        if (onChange) {
            onChange(value);
            if (!autoSet) {
                this.hideModal();
            }
        }

        if (field && _.get(field, 'dependent.enabled', false)) {
            onDependentValueUpdate(
                getFieldAttribute(_.get(field, 'dependent.name'), 'actionToUse'),
                value
            );
        }
    }

    updateItems() {
        const { field, includeEmptyItem = true } = this.props;

        const items = getItemsForPicker(field, includeEmptyItem);

        this.setState((state) => {
            if (_.get(state, 'searchTerm')) {
                this.search();
            }

            return {
                items
            };
        });

        const value = _.get(field, value);

        if (_.isArray(items) && items.length === 1) {
            this.onValueChange(_.get(_.nth(items, 0), 'value'), true);
        }
    }
}

export default withTheme(Picker);

Picker.propTypes = {
    field: PropTypes.object,
    searchable: PropTypes.bool,
    searchPlaceholder: PropTypes.string,
    includeEmptyItem: PropTypes.bool,

    theme: PropTypes.object,
    containerStyle: ViewPropTypes.style,
    inputContainerStyle: ViewPropTypes.style,
    inputTextStyle: ViewPropTypes.style,
    placeholderTextStyle: ViewPropTypes.style,
    pickerTitleContainerStyle: ViewPropTypes.style,
    closeButtonContainerStyle: ViewPropTypes.style,
    closeButtonTextStyle: ViewPropTypes.style,
    modalContainerStyle: ViewPropTypes.style,
    modalStyle: ViewPropTypes.style,
    titleTextStyle: ViewPropTypes.style,
    selectedIconStyle: ViewPropTypes.style,
    labelStyle: PropTypes.object,
    addItemButtonContainerStyle: ViewPropTypes.style,
    addItemButtonStyle: ViewPropTypes.style,
    addItemButtonTitleStyle: ViewPropTypes.style,

    onChange: PropTypes.func,
    getFieldAttribute: PropTypes.func,
    renderModal: PropTypes.func,
    onAddItem: PropTypes.func
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        paddingHorizontal: 10
    },
    inputContainerStyle: {
        fontSize: 16,
        paddingLeft: 14,
        paddingRight: 14,
        paddingTop: 10,
        paddingBottom: 10
    },
    placeholderTextStyle: {
        color: colors.grey
    },
    inputTextStyle: {
        color: colors.black,
        lineHeight: 16
    },
    touchableOpacity: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dropdownIcon: {
        flex: 1,
        fontSize: 16,
        color: colors.black
    },
    spinnerView: {
        margin: 20
    },
    noResultsText: {
        margin: 20,
        textAlign: 'center',
        backgroundColor: colors.transparent
    },
    pickerTitleContainerStyle: {
        flexDirection: 'column',
        backgroundColor: colors.black,
        paddingBottom: 10,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    flatListStyle: {
        maxHeight: '85%',
        minHeight: '20%',
        flexGrow: 0
    },
    modalContainerStyle: {
        flex: 1,
        backgroundColor: colors.black30,
        justifyContent: 'center'
    },
    modalStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        width: '90%',
        borderRadius: 5,
        maxHeight: '80%',
        backgroundColor: colors.white
    },
    listRowContainerStyle: {
        width: '100%',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center'
    },
    listTextViewStyle: {
        color: colors.black,
        marginVertical: 10,
        flex: 0.9,
        marginLeft: 20,
        marginHorizontal: 10,
        textAlign: 'left'
    },
    listRowClickTouchStyle: {
        justifyContent: 'center',
        flexDirection: 'row',
        flex: 1
    },
    pickerTitleTextStyle: {
        paddingTop: 14,
        paddingLeft: 30,
        color: colors.black,
        fontSize: 16,
        fontWeight: '400'
    },
    searchBar: {
        marginTop: 4
    },
    selectedIconStyle: {
        fontSize: Platform.OS === 'ios' ? 30 : 25,
        color: colors.black
    },
    closeButtonContainerStyle: {
        backgroundColor: colors.black30,
        borderRadius: 4,
        width: 46,
        height: 22,
        position: 'absolute',
        top: 10,
        right: 10,
        justifyContent: 'center'
    },
    closeButtonTextStyle: {
        color: colors.white,
        alignSelf: 'center'
    },
    addItemButtonContainerStyle: {
        marginBottom: 5
    },
    addItemButtonStyle: {
        backgroundColor: colors.transparent
    },
    addItemButtonTitleStyle: {
        color: colors.backgroundDarkGrey,
        fontSize: 14
    }
});
