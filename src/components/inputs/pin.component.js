import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Input, withTheme } from 'react-native-elements';
import PropTypes from 'prop-types';
import colors from '../../theme/colors';
import * as _ from 'lodash';
import { getDisabledStyle, getIcon, getLabel, getValidationIcon } from '../../helpers/form.helper';

class Pin extends Component {
    render() {
        const { field, onChange, theme, length = 4, labelStyle = {} } = this.props;

        const enabled = _.get(field, 'enabled', false);
        const labelProps = getLabel(field, labelStyle);

        return (
            <Input
                leftIcon={getIcon(_.get(field, 'icon'))}
                rightIcon={getValidationIcon(field)}
                placeholder={_.get(field, 'placeholder')}
                placeholderTextColor={colors.placeholder}
                keyboardType="decimal-pad"
                style={[styles.input, getDisabledStyle(enabled)]}
                value={_.get(field, 'value')}
                maxLength={length}
                secureTextEntry
                onChangeText={onChange}
                returnKeyType="done"
                editable={enabled}
                errorStyle={[styles.errorStyle, _.get(theme, 'Input.errorStyle')]}
                errorMessage={_.get(field, 'error')}
                {...labelProps}
            />
        );
    }
}

export default withTheme(Pin);

Pin.propTypes = {
    field: PropTypes.object,
    length: PropTypes.number,

    theme: PropTypes.object,
    labelStyle: PropTypes.object,

    onChange: PropTypes.func
};

const styles = StyleSheet.create({
    input: { fontSize: 16, paddingLeft: 14 },
    errorStyle: {
        color: colors.error
    }
});
