import React, { Component } from 'react';
import { Input, withTheme } from 'react-native-elements';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import * as _ from 'lodash';

import { getDisabledStyle, getIcon, getLabel, getValidationIcon } from '../../helpers/form.helper';
import colors from '../../theme/colors';

class Default extends Component {
    render() {
        const {
            field,
            onChange,
            theme,
            multiline = false,
            numberOfLines = 1,
            labelStyle = {}
        } = this.props;

        const enabled = _.get(field, 'enabled', true);
        const labelProps = getLabel(field, labelStyle);

        return (
            <Input
                leftIcon={getIcon(_.get(field, 'icon'))}
                rightIcon={getValidationIcon(field)}
                placeholder={_.get(field, 'placeholder')}
                placeholderTextColor={colors.placeholder}
                style={[styles.input, getDisabledStyle(enabled)]}
                value={_.get(field, 'value')}
                onChangeText={onChange}
                editable={enabled}
                errorStyle={[styles.errorStyle, _.get(theme, 'Input.errorStyle')]}
                errorMessage={_.get(field, 'error')}
                multiline={multiline}
                numberOfLines={numberOfLines}
                {...labelProps}
            />
        );
    }
}

export default withTheme(Default);

Default.propTypes = {
    field: PropTypes.object,
    theme: PropTypes.object,
    multiline: PropTypes.bool,
    numberOfLines: PropTypes.number,

    labelStyle: PropTypes.object,

    onChange: PropTypes.func
};

const styles = StyleSheet.create({
    errorStyle: {
        color: colors.error
    },
    input: {
        paddingTop: 0,
        paddingBottom: 0
    }
});
