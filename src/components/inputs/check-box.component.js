import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { boundMethod } from 'autobind-decorator';
import ToggleSwitch from 'toggle-switch-react-native';
import { ViewPropTypes } from 'react-native';
import { withTheme } from 'react-native-elements';
import InputWrapper from '../utility/input-wrapper.component';

class CheckBox extends Component {
    render() {
        const { field, containerStyle, theme, inputContainerStyle, labelStyle } = this.props;

        return (
            <InputWrapper
                theme={theme}
                field={field}
                containerStyle={containerStyle}
                inputContainerStyle={inputContainerStyle}
                labelStyle={labelStyle}
            >
                <ToggleSwitch
                    isOn={_.get(field, 'value', false)}
                    label={_.get(field, 'placeholder', '')}
                    onToggle={this.onChange}
                />
            </InputWrapper>
        );
    }

    @boundMethod
    onChange(isOn) {
        const { field, onChange } = this.props;

        if (onChange && _.get(field, 'enabled', true)) {
            onChange(isOn);
        }
    }
}

export default withTheme(CheckBox);

CheckBox.propTypes = {
    field: PropTypes.object,
    theme: PropTypes.object,

    containerStyle: ViewPropTypes.style,
    inputContainerStyle: ViewPropTypes.style,
    labelStyle: PropTypes.object,

    onChange: PropTypes.func
};
