import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import { boundMethod } from 'autobind-decorator';
import * as _ from 'lodash';
import moment from 'moment';
import { Icon, withTheme } from 'react-native-elements';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Label from '../form/label.component';
import Error from '../utility/error.component';
import colors from '../../theme/colors';

class DateTimePicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalOpen: false
        };
    }

    render() {
        const {
            field,
            mode = 'date',
            containerStyle,
            inputContainerStyle,
            labelStyle,
            theme
        } = this.props;

        const { modalOpen } = this.state;

        const enabled = _.get(field, 'enabled', false);

        const icon = _.get(field, 'icon', {});

        let disabledStyle = {};
        if (!enabled) {
            disabledStyle = { opacity: 0.25 };
        }

        return (
            <View
                style={[
                    styles.containerStyle,
                    _.get(theme, 'Input.containerStyle'),
                    containerStyle,
                    disabledStyle
                ]}
            >
                <Label
                    label={_.get(field, 'label')}
                    required={_.get(field, 'required', false)}
                    labelStyle={labelStyle}
                />
                <TouchableOpacity
                    disabled={!enabled}
                    onPress={this.toggleModal}
                    style={[
                        styles.inputContainerStyle,
                        _.get(theme, 'Input.inputContainerStyle'),
                        inputContainerStyle
                    ]}
                >
                    <View style={styles.touchableOpacity}>
                        {this.renderValue()}
                        <Icon {...icon} style={styles.dropdownIcon} />
                    </View>
                </TouchableOpacity>
                <Error error={_.get(field, 'error')} />

                <DateTimePickerModal
                    isVisible={modalOpen}
                    date={this.getDateValue()}
                    mode={mode}
                    is24Hour
                    display="default"
                    disabled={!enabled}
                    onConfirm={this.onChange}
                    onCancel={this.toggleModal}
                />
            </View>
        );
    }

    renderValue() {
        const { field, inputTextStyle, placeholderTextStyle, theme } = this.props;
        const selectedValue = _.get(field, 'value');

        if (!selectedValue) {
            return (
                <Text
                    style={[
                        styles.inputTextStyle,
                        styles.placeholderTextStyle,
                        _.get(theme, 'Input.placeholderTextStyle'),
                        placeholderTextStyle
                    ]}
                >
                    {_.get(field, 'placeholder')}
                </Text>
            );
        }

        return (
            <Text
                style={[
                    styles.inputTextStyle,
                    _.get(theme, 'Input.inputTextStyle'),
                    inputTextStyle
                ]}
            >
                {this.getStringValue(selectedValue)}
            </Text>
        );
    }

    @boundMethod
    toggleModal() {
        this.setState((prevState) => ({
            modalOpen: !prevState.modalOpen
        }));
    }

    @boundMethod
    onChange(value) {
        const { onChange } = this.props;

        if (onChange) {
            this.toggleModal();
            onChange(this.getStringValue(value));
        }
    }

    getStringValue(value) {
        const { format = 'YYYY-MM-DD' } = this.props;

        const date = moment(value);

        return date.format(format);
    }

    getDateValue() {
        const { field } = this.props;

        let date = _.get(field, 'value');

        if (_.isNil(date) || _.isEmpty(date)) {
            date = new Date();
        }

        return moment(date).toDate();
    }
}

export default withTheme(DateTimePicker);

DateTimePicker.propTypes = {
    format: PropTypes.string,
    field: PropTypes.object,
    styles: PropTypes.object,
    mode: PropTypes.string,
    theme: PropTypes.object,

    containerStyle: ViewPropTypes.style,
    inputContainerStyle: ViewPropTypes.style,
    inputTextStyle: ViewPropTypes.style,
    placeholderTextStyle: ViewPropTypes.style,
    labelStyle: PropTypes.object,

    onChange: PropTypes.func
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        paddingHorizontal: 10
    },
    inputContainerStyle: {
        fontSize: 16,
        paddingLeft: 14,
        paddingRight: 14,
        paddingTop: 10,
        paddingBottom: 10
    },
    inputTextStyle: {
        color: colors.black,
        lineHeight: 16
    },
    touchableOpacity: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dropdownIcon: {
        flex: 1,
        fontSize: 22,
        color: colors.black
    },
    placeholderTextStyle: {
        fontSize: 16,
        color: colors.darkGreyText,
        paddingLeft: 14,
        alignSelf: 'flex-start'
    }
});
