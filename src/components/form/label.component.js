import React, { Component } from 'react';
import { StyleSheet, Text, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';

import colors from '../../theme/colors';
import { withTheme } from 'react-native-elements';

class Label extends Component {
    render() {
        const { label, required = true, labelStyle, requiredStyle, theme } = this.props;

        if (!label) {
            return null;
        }

        return (
            <Text style={[styles.labelStyle, _.get(theme, 'Input.labelStyle'), labelStyle]}>
                {label}
                &nbsp;
                {required && (
                    <Text
                        style={[
                            styles.requiredStyle,
                            _.get(theme, 'Label.requiredStyle'),
                            requiredStyle
                        ]}
                    >
                        *
                    </Text>
                )}
            </Text>
        );
    }
}

export default withTheme(Label);

const styles = StyleSheet.create({
    labelStyle: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    requiredStyle: {
        color: colors.required
    }
});

Label.propTypes = {
    label: PropTypes.string,
    required: PropTypes.bool,
    theme: PropTypes.object,

    requiredStyle: ViewPropTypes.style,
    labelStyle: PropTypes.object
};
