import React, { Component } from 'react';
import { StyleSheet, Text, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import colors from '../../theme/colors';

export default class SubmitButton extends Component {
    render() {
        const {
            isLoading,
            title,
            buttonStyle,
            titleStyle,
            onPress,
            disabled,
            disabledButtonStyle
        } = this.props;
        const customButtonStyle = { ...styles.buttonStyle, ...buttonStyle };
        const customTitleStyle = { ...styles.titleStyle, ...titleStyle };

        return (
            <Button
                disabled={isLoading || disabled}
                disabledStyle={[
                    styles.buttonStyle,
                    buttonStyle,
                    styles.disabledButtonStyle,
                    disabledButtonStyle
                ]}
                block
                loading={isLoading}
                title={title}
                onPress={onPress}
                titleStyle={customTitleStyle}
                buttonStyle={customButtonStyle}
            />
        );
    }
}

SubmitButton.propTypes = {
    isLoading: PropTypes.bool,
    title: PropTypes.string,
    disabled: PropTypes.bool,

    buttonStyle: PropTypes.object,
    titleStyle: Text.propTypes.style,
    disabledButtonStyle: ViewPropTypes.style,

    onPress: PropTypes.func
};

const styles = StyleSheet.create({
    titleStyle: {
        color: colors.white
    },
    buttonStyle: {
        borderRadius: 30,
        width: 200,
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 20,
        zIndex: 0
    },
    disabledButtonStyle: {
        backgroundColor: colors.backgroundDarkGrey
    }
});
