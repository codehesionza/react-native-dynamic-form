import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
import { boundMethod } from 'autobind-decorator';
import _ from 'lodash';

export default class Field extends Component {
    constructor(props) {
        super(props);

        this.debouncedValidate = _.debounce(this.onValidate, 1000);
    }

    static getItemValidationStyle(field) {
        if (field.value || (!field.value && field.error)) {
            return field.error ? { error: true } : { success: true };
        }

        return {};
    }

    componentDidMount() {
        const { field = { value: undefined } } = this.props;
        if (field.value) {
            this.onValueChange(field.value);
        }
    }

    /**
     * Renders the field.
     *
     * @returns {null|*}
     */
    render() {
        const { field } = this.props;

        if (!field) {
            return null;
        }

        return (
            <View key={field.name}>
                <View {...Field.getItemValidationStyle(field)} style={styles.inputItemWrapper}>
                    {this.renderComponent()}
                </View>
            </View>
        );
    }

    /**
     * Renders the field's component
     *
     * @returns {Component}
     */
    renderComponent() {
        const { field } = this.props;

        return _.has(field, 'render')
            ? field.render({
                  ...this.props,
                  ..._.get(field, 'customStyles', {}),
                  onChange: this.onValueChange
              })
            : null;
    }

    /**
     * Run the on change function when the value changes.
     *
     * @param {mixed} value
     */
    @boundMethod
    onValueChange(value) {
        const { field, onChange } = this.props;

        if (onChange) {
            onChange(field.name, value);
        }

        this.debouncedValidate(value);
    }

    /**
     * Validates the field.
     *
     * @param {mixed} value
     */
    @boundMethod
    onValidate(value) {
        const { field, onValidate } = this.props;

        if (onValidate) {
            onValidate(field.name, value);
        }
    }
}

Field.propTypes = {
    field: PropTypes.object,
    labelStyle: PropTypes.object,

    onChange: PropTypes.func,
    onValidate: PropTypes.func,
    getFieldAttribute: PropTypes.func
};

const styles = StyleSheet.create({
    inputItemWrapper: {
        flexDirection: 'row',
        marginRight: 10
    }
});
