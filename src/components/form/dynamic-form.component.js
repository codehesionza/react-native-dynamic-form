import React, { Component } from 'react';
import { StyleSheet, View, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import { boundMethod } from 'autobind-decorator';
import ErrorSummary from '../utility/error-summary.component';
import SubmitButton from './submit-button.component';
import {
    getExtraValidationParams,
    validateField,
    validateFields
} from '../../helpers/validate.helper';
import _ from 'lodash';
import { extractReturnObjectFromFields } from '../../helpers/form.helper';
import { ThemeProvider } from 'react-native-elements';
import Field from './field.component';

export default class DynamicForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fields: props.fields || [],
            submitEnabled: false
        };
    }

    componentDidMount() {
        const { data = {} } = this.props;
        this.setState(
            (prevState, props) => ({
                fields: props.fields
            }),
            () => {
                this.loadData(data);
            }
        );
    }

    componentDidUpdate(prevProps) {
        if (this.props.fields !== prevProps.fields) {
            this.setState((prevState, props) => ({
                fields: props.fields
            }));
        }

        if (this.props.data !== prevProps.data) {
            this.loadData(_.get(this.props, 'data'));
        }
    }

    render() {
        const { theme } = this.props;

        return <ThemeProvider theme={theme}>{this.renderForm()}</ThemeProvider>;
    }

    renderForm() {
        const { render } = this.props;

        if (render) {
            return render({
                fields: this.renderFields(),
                submitButton: this.renderSubmitButton(),
                errorSummary: this.renderErrorSummary()
            });
        }

        return (
            <View>
                {this.renderFields()}
                {this.renderErrorSummary()}
                {this.renderSubmitButton()}
            </View>
        );
    }

    renderFields() {
        const { containerStyle, inputContainerStyle, labelStyle } = this.props;
        const { fields = [] } = this.state;

        return (
            <View>
                {fields.map(
                    (field) =>
                        this.shouldDisplayField(field) && (
                            <Field
                                key={_.get(field, 'name')}
                                field={field}
                                onValidate={this.onValidate}
                                onChange={this.onFieldChange}
                                getFieldAttribute={this.getFieldAttribute}
                                styles={{ labelStyle, containerStyle, inputContainerStyle }}
                            />
                        )
                )}
            </View>
        );
    }

    renderSubmitButton() {
        const { submitting = false, submit } = this.props;
        const { submitEnabled } = this.state;

        const label = _.get(this.props, 'submit.label', 'Submit');

        return (
            <SubmitButton
                isLoading={submitting}
                onPress={this.onSubmit}
                title={label}
                style={styles.submitButton}
                disabled={!submitEnabled}
                {...submit}
            />
        );
    }

    renderErrorSummary() {
        const { error, showErrorSummary = false } = this.props;

        return showErrorSummary && <ErrorSummary error={error} />;
    }

    loadData(data) {
        if (data) {
            this.setState(
                (prevState) => ({
                    fields: prevState.fields.map((field) => {
                        if (_.has(data, field.name)) {
                            field.value = _.get(data, field.name);
                        }

                        return field;
                    })
                }),
                this.setCanSubmit
            );
        }
    }

    @boundMethod
    onValidate(name, value) {
        const { fields = [] } = this.state;
        const field = this.getField(name);
        const validationParams = _.merge({ [name]: value }, getExtraValidationParams(fields));
        const error = validateField(name, validationParams, _.get(field, 'validation'));

        this.setState(
            (prevState) => ({
                fields: prevState.fields.map((field) => {
                    if (field.name === name) {
                        field.error = error;
                    }

                    return field;
                })
            }),
            this.setCanSubmit
        );
    }

    @boundMethod
    onFieldChange(name, value) {
        const { onChange } = this.props;
        const field = this.getField(name);

        this.setState(
            (prevState) => {
                let foundIndex;

                return {
                    fields: prevState.fields.map((field, index) => {
                        if (field.name === name) {
                            let computedValue = value;
                            if (_.has(field, 'modifier') && typeof field.modifier === 'function') {
                                computedValue = field.modifier(value);
                            }
                            foundIndex = index;
                            field.value = computedValue;
                        }

                        if (!_.get(field, 'enabled', false)) {
                            field.enabled = _.eq(index, foundIndex + 1);
                        }

                        return field;
                    })
                };
            },
            () => {
                this.setCanSubmit();
                if (onChange) {
                    onChange(field);
                }
            }
        );
    }

    /**
     *
     * @param {string} name
     * @returns {Object}
     */
    getField(name) {
        const { fields = [] } = this.state;

        return fields.find((field) => field.name === name);
    }

    /**
     *
     * @param {string} name
     * @param {string} attribute
     * @returns {Object | undefined}
     */
    @boundMethod
    getFieldAttribute(name, attribute) {
        const field = this.getField(name);

        return field ? field[attribute] : undefined;
    }

    /**
     * Sets, processes and returns the final data object for submission
     * (set from the values contained within the field objects).
     *
     * @returns {Object}
     */
    getReturnObject() {
        return extractReturnObjectFromFields(this.state.fields);
    }

    @boundMethod
    onSubmit() {
        const { onAction } = this.props.submit;

        const errors = this.validateForm();

        this.checkErrors(errors).then((hasErrors) => {
            if (!hasErrors) {
                onAction(this.getReturnObject());
                this.onClear();
            }
        });
    }

    @boundMethod
    onClear() {
        const { clearAfterSubmit = false } = this.props;
        const { fields = [] } = this.state;

        if (clearAfterSubmit) {
            const emptyData = fields.reduce((data, field) => {
                data[field.name] = _.get(field, 'defaultValue');
                return data;
            }, {});

            this.loadData(emptyData);
        }
    }

    /**
     * Validates the form and returns null or returns an object of errors.
     *
     * @returns {Object}
     */
    validateForm() {
        const { fields = [] } = this.state;
        return validateFields(
            _.reduce(
                fields,
                (final, field) => {
                    if (this.shouldDisplayField(field)) {
                        final.push(field);
                    }

                    return final;
                },
                []
            )
        );
    }

    /**
     * Checks if this form should display the given field or not.
     *
     * @param {Object} field
     * @returns {boolean}
     */
    shouldDisplayField(field) {
        const { sceneKey } = this.props;

        return _.has(field, 'display') && typeof field.display === 'function'
            ? field.display(sceneKey)
            : true;
    }

    /**
     * Sets the errors contained in the errors object parameter.
     *
     * @param {Object} errors
     */
    checkErrors(errors) {
        return new Promise((resolve) => {
            if (errors) {
                this.setState(
                    (prevState) => ({
                        fields: prevState.fields.map((field) => {
                            if (_.has(errors, field.name)) {
                                field.error = _.nth(_.get(errors, field.name), 0);
                            } else {
                                field.error = undefined;
                            }

                            return field;
                        })
                    }),
                    () => {
                        this.setCanSubmit();
                        resolve(true);
                    }
                );
            } else {
                resolve(false);
            }
        });
    }

    setCanSubmit() {
        this.setState((prevState) => ({
            submitEnabled: prevState.fields.reduce((status, field) => {
                return status && _.isNil(_.get(field, 'error'));
            }, true)
        }));
    }
}

const styles = StyleSheet.create({
    submitButton: {
        marginTop: 20,
        marginBottom: 10,
        width: '55%',
        borderRadius: 30,
        alignSelf: 'center'
    }
});

DynamicForm.propTypes = {
    data: PropTypes.object,
    fields: PropTypes.array,
    submitting: PropTypes.bool,
    error: PropTypes.instanceOf(Error),
    showErrorSummary: PropTypes.bool,
    theme: PropTypes.object,
    sceneKey: PropTypes.string,
    submit: PropTypes.shape({
        label: PropTypes.string,
        onAction: PropTypes.func
    }),
    clearAfterSubmit: PropTypes.bool,

    containerStyle: ViewPropTypes.style,
    inputContainerStyle: ViewPropTypes.style,
    labelStyle: PropTypes.object,

    onChange: PropTypes.func,
    render: PropTypes.func
};
