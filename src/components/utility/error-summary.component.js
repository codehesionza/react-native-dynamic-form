import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Text } from 'react-native-elements';
import { View } from 'react-native';

import colors from '../../theme/colors';

export default class ErrorSummary extends Component {
    render() {
        const {
            error = {},
            styles = {
                errorHeading: {
                    color: colors.failedRed
                },
                errorTextStyle: {
                    fontSize: 14
                },
                cardStyle: {
                    borderWidth: 1,
                    borderColor: colors.failedRed
                }
            }
        } = this.props;
        const { errors = [] } = error;

        if (!error || errors.length === 0) {
            return null;
        }

        return (
            <Card containerStyle={styles.cardStyle} title="Error Summary">
                {errors.map((error, index) => (
                    <View key={index}>
                        <Text style={styles.errorTextStyle}>- {error}</Text>
                    </View>
                ))}
            </Card>
        );
    }
}

ErrorSummary.propTypes = {
    styles: PropTypes.object,
    error: PropTypes.instanceOf(Error)
};
