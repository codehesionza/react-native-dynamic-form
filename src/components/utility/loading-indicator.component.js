import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

export default class LoadingIndicator extends Component {
    render() {
        const { loading = false, size = 'small', containerStyle = {} } = this.props;
        return (
            <View style={[styles.container, styles.horizontal, containerStyle]}>
                <ActivityIndicator animating={loading} size={size} />
            </View>
        );
    }
}

LoadingIndicator.propTypes = {
    loading: PropTypes.bool,
    size: PropTypes.string,
    containerStyle: PropTypes.object
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});
