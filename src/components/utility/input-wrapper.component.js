import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ViewPropTypes } from 'react-native';
import { withTheme } from 'react-native-elements';
import _ from 'lodash';

import { getDisabledStyle, getIcon } from '../../helpers/form.helper';
import Error from './error.component';
import Label from '../form/label.component';

class InputWrapper extends Component {
    render() {
        const {
            field,
            theme,
            children,
            containerStyle,
            inputContainerStyle,
            labelStyle
        } = this.props;

        const enabled = _.get(field, 'enabled', true);

        return (
            <View
                style={[
                    styles.containerStyle,
                    _.get(theme, 'Input.containerStyle'),
                    containerStyle
                ]}
            >
                <Label
                    required={_.get(field, 'required', false)}
                    label={_.get(field, 'label')}
                    labelStyle={labelStyle}
                />
                <View
                    style={[
                        styles.inputContainerStyle,
                        _.get(theme, 'Input.inputContainerStyle'),
                        inputContainerStyle,
                        getDisabledStyle(enabled)
                    ]}
                >
                    {_.has(field, 'icon') && getIcon(_.get(field, 'icon'))}
                    {children}
                </View>
                <Error error={_.get(field, 'error')} />
            </View>
        );
    }
}

export default withTheme(InputWrapper);

InputWrapper.propTypes = {
    field: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),

    theme: PropTypes.object,
    containerStyle: ViewPropTypes.style,
    inputContainerStyle: ViewPropTypes.style,
    labelStyle: PropTypes.object
};

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        paddingHorizontal: 10
    },
    inputContainerStyle: {
        fontSize: 16,
        paddingLeft: 14,
        paddingTop: 10,
        paddingBottom: 10
    }
});
