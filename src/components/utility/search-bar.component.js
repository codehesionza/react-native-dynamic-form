import React, { Component } from 'react';
import { Button, Icon, Input } from 'react-native-elements';
import { boundMethod } from 'autobind-decorator';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import _ from 'lodash';

import colors from '../../theme/colors';

export default class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.debouncedSearch = _.debounce(this.search, 1000);
    }

    render() {
        const { placeholder = 'Search', searchTerm, searchBarContainerStyle } = this.props;

        return (
            <View rounded style={[styles.searchBarContainerStyle, searchBarContainerStyle]}>
                <Input
                    leftIcon={{ name: 'search' }}
                    leftIconStyle={styles.iconStyle}
                    style={styles.inputText}
                    placeholder={placeholder}
                    placeholderTextColor={colors.placeholder}
                    onSubmitEditing={this.search}
                    onEndEditing={this.search}
                    onChangeText={this.setSearchTerm}
                    value={searchTerm}
                    returnKeyType="search"
                />
                {searchTerm ? this.renderCancelButton() : null}
            </View>
        );
    }

    renderCancelButton() {
        return (
            <Button transparent onPress={this.cancelSearch} style={styles.cancelButton}>
                <Icon active name="close-circle" style={styles.iconStyle} />
            </Button>
        );
    }

    @boundMethod
    setSearchTerm(newSearchTerm) {
        const { setSearchTermAction } = this.props;
        if (setSearchTermAction) {
            setSearchTermAction(newSearchTerm);
        }

        this.debouncedSearch();
    }

    @boundMethod
    search() {
        const { onSearch } = this.props;

        if (onSearch) {
            onSearch();
        }
    }

    @boundMethod
    cancelSearch() {
        const { onSearch, setSearchTermAction } = this.props;

        if (setSearchTermAction) {
            setSearchTermAction('');
        }
        if (onSearch) {
            onSearch();
        }
    }
}

SearchBar.propTypes = {
    placeholder: PropTypes.string,
    searchTerm: PropTypes.string,
    searchBarContainerStyle: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
        PropTypes.array
    ]),

    onSearch: PropTypes.func,
    setSearchTermAction: PropTypes.func
};

const styles = StyleSheet.create({
    searchBarContainerStyle: {
        margin: 10,
        height: 40,
        borderColor: colors.backgroundDarkGrey
    },
    iconStyle: {
        fontSize: 20,
        color: colors.placeholder,
        paddingTop: 0
    },
    inputText: {
        fontSize: 14,
        color: colors.placeholder,
        paddingLeft: 0
    },
    cancelButton: {
        paddingTop: 0,
        paddingBottom: 3
    }
});
