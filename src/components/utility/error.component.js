import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { withTheme } from 'react-native-elements';
import PropTypes from 'prop-types';
import _ from 'lodash';

import colors from '../../theme/colors';

/**
 *
 */
class Error extends Component {
    render() {
        const { error, theme } = this.props;
        if (!error) {
            return null;
        }

        return <Text style={[styles.errorStyle, _.get(theme, 'Input.errorStyle')]}>{error}</Text>;
    }
}

export default withTheme(Error);

Error.propTypes = {
    error: PropTypes.string,

    theme: PropTypes.object
};

const styles = StyleSheet.create({
    errorStyle: {
        color: colors.error
    }
});
