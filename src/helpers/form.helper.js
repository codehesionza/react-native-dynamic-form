import _ from 'lodash';
import React from 'react';
import { Icon } from 'react-native-elements';
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';
import Label from '../components/form/label.component';

/**
 *
 * @param {boolean} enabled
 * @returns {null|{opacity: number}}
 */
export const getDisabledStyle = (enabled) => {
    if (enabled) {
        return null;
    }

    return {
        opacity: 0.25
    };
};

/**
 *
 * @param {Array} fields
 * @returns {Object}
 */
export const extractReturnObjectFromFields = (fields) => {
    return fields.reduce((data, field) => {
        data[field.name] = _.get(field, 'value');

        return data;
    }, {});
};

/**
 *
 * @param {Array} fields
 * @param {string} name
 * @returns {Object}
 */
export const getField = (fields, name) => {
    return fields.find((field) => field.name === name);
};

/**
 *
 * @param {Object} icon
 * @param {Object} style
 * @returns {null|*}
 */
export const getIcon = (icon, style = {}) => {
    if (icon) {
        return (
            <Icon
                name={_.get(icon, 'name')}
                type={_.get(icon, 'type', 'font-awesome')}
                iconStyle={[defaultStyle.iconStyle, style]}
            />
        );
    }

    return null;
};

export const getLabel = (field, labelStyle) => {
    return {
        label: (
            <Label
                label={_.get(field, 'label')}
                required={_.get(field, 'required', true)}
                labelStyle={labelStyle}
            />
        )
    };
};

/**
 *
 * @param {Object} field
 * @param {string} type
 * @returns {null|*}
 */
export const getValidationIcon = (field, type = 'font-awesome') => {
    if (field.value || (!field.value && field.error)) {
        return (
            <Icon
                name={field.error ? 'times-circle-o' : 'check-circle-o'}
                type={type}
                iconStyle={field.error ? defaultStyle.errorStyle : defaultStyle.successStyle}
            />
        );
    }
    return null;
};

/**
 *
 * @type {StyleSheet.NamedStyles<T> | StyleSheet.NamedStyles<any>}
 */
const defaultStyle = StyleSheet.create({
    iconStyle: {
        color: colors.white
    },
    successStyle: {
        color: colors.success
    },
    errorStyle: {
        color: colors.error
    }
});
