/**
 *
 * @param {string} reducerName
 * @param {string} actionName
 * @returns {string}
 */
export const buildActionType = (reducerName = '', actionName = '') => {
    reducerName = reducerName.toString().trim();
    if (!reducerName) {
        throw new Error('Reducer name cannot be blank');
    }
    actionName = actionName.toString().trim();
    if (!actionName) {
        throw new Error('Action name cannot be blank');
    }
    return `${reducerName}/${actionName}`;
};

/**
 *
 * @param {string} reducerName
 * @param {string} actionName
 * @returns {{actionType: string, action: (function(*): {payload: Object, type: string})}}
 */
export const createAction = (reducerName, actionName) => {
    if (!reducerName || !actionName) {
        throw new Error('Please provide a valid reducer and action name');
    }
    const actionType = buildActionType(reducerName, actionName);
    return {
        actionType,
        action: (payload) => ({
            type: actionType,
            payload
        })
    };
};
