import { validate } from 'validate.js';
import _ from 'lodash';
import { getField } from './form.helper';

const options = {
    fullMessages: true
};

export const validateField = (fieldName, formValues, validationRules = {}, options = options) => {
    // e.g. const formFields = {
    //                        email: {
    //                         presence: {
    //                          message: 'Email is blank'
    //                         }
    //                       }
    const formFields = {};
    formFields[fieldName] = validationRules;

    // The formValues and validated against the formFields
    // the variable result hold the error messages of the field
    const result = validate(formValues, formFields, options);

    // If there is an error message, return it!
    if (result) {
        // Return only the field error message if there are multiple
        return _.nth(_.get(result, fieldName), 0);
    }

    return null;
};

export const validateFields = (fields = []) => {
    const formFields = {};
    const formValues = {};
    fields.forEach((field) => {
        formFields[field.name] = field.validation;
        formValues[field.name] = field.value;
    });

    // The formValues and validated against the formFields
    // the variable result hold the error messages of the field
    return validate(formValues, formFields, options);
};

export const getExtraValidationParams = (fields = []) => {
    return fields.reduce((params, field) => {
        if (_.has(field, 'validation.equality')) {
            const fieldToCompare = getField(fields, _.get(field, 'validation.equality.attribute'));
            if (fieldToCompare) {
                params[_.get(fieldToCompare, 'name')] = _.get(fieldToCompare, 'value');
            }
        }

        return params;
    }, {});
};

export const validators = {
    currency: (message = '^Please enter a valid monetary amount.') => ({
        format: { pattern: /^\d+(\.\d{2})?$/, flags: 'i', message }
    }),
    phoneNumber: (message = '^Please use only digits.', fieldName = 'Phone Number') => ({
        length: {
            minimum: 9,
            maximum: 15,
            tooShort: `${fieldName} is too short (minimum is %{count} digits).`,
            tooLong: `${fieldName} is too long (maximum is %{count} digits).`
        },
        format: {
            pattern: /[0-9]*/,
            message
        }
    }),
    decimal: (message = '^Please enter a valid decimal amount.') => ({
        numericality: {
            message
        }
    }),
    pin: (length = 4, message = '^Please use only digits.') => ({
        presence: {
            allowEmpty: false,
            message: 'Cannot be empty.'
        },
        length: {
            minimum: length,
            maximum: length,
            message: `Must be ${length} digits long.`
        },
        format: {
            pattern: /[0-9]*/,
            message
        }
    })
};
