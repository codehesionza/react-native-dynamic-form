/**
 *
 * @param {string} haystack
 * @param {string} needle
 * @returns {boolean}
 */
export const contains = (haystack, needle) => {
    return haystack.indexOf(needle) > -1;
};
