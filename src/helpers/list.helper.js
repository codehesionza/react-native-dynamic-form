import _ from 'lodash';

/**
 * Adds an empty item to an array for a drop down list.
 *
 * @param {string} idField
 * @param {string} nameField
 * @param {string} placeholder
 * @returns {*[]}
 */
export const getEmptyList = ({ idField = 'id', nameField = 'name', placeholder = 'Element' }) => {
    const object = {};
    object[idField] = undefined;
    object[nameField] = 'Please select a ' + placeholder;

    return [object];
};

export const getItemsForPicker = (field, includeEmptyItem = true) => {
    const originalSource = _.has(field, 'source') ? field.source() : field.items;
    const nameKey = _.get(field, 'itemData.nameField', 'name');
    const idKey = _.get(field, 'itemData.idField', 'id');

    let items = [];

    if (includeEmptyItem) {
        items = getEmptyList({
            idField: idKey,
            nameField: nameKey,
            placeholder: _.get(field, 'label')
        });
    }

    items = items.concat(originalSource);

    return _.map(items, (item, index) => ({
        key: _.get(item, idKey) || index,
        label: _.get(item, nameKey),
        value: _.get(item, idKey)
    }));
};

export const onDependentValueUpdate = (actionToUse, value) => {
    actionToUse(value);
};
