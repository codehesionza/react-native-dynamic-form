import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { Field } from '../types/types';
import Number from '../components/inputs/number.component';

export const NumberRenderer = (props) => {
    const { field, onChange, styles = {} } = props;

    return <Number field={field} onChange={onChange} keyboardType="decimal-pad" {...styles} />;
};

NumberRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
