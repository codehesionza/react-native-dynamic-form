import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { Field } from '../types/types';
import CheckBox from '../components/inputs/check-box.component';

export const CheckBoxRenderer = (props) => {
    const { field, onChange, styles = {} } = props;

    return <CheckBox field={field} onChange={onChange} {...styles} />;
};

CheckBoxRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
