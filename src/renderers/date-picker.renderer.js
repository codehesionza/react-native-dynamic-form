import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { Field } from '../types/types';
import DateTimePicker from '../components/inputs/date-time-picker.component';

export const DatePickerRenderer = (props) => {
    const { field, onChange, styles = {} } = props;

    return <DateTimePicker field={field} mode="date" onChange={onChange} {...styles} />;
};

DatePickerRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
