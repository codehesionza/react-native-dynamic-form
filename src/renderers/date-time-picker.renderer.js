import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import DateTimePicker from '../components/inputs/date-time-picker.component';
import { Field } from '../types/Field';

export const DateTimePickerRenderer = (props) => {
    const { field, onChange, styles = {} } = props;

    return (
        <DateTimePicker
            field={field}
            mode="datetime"
            format="YYYY-MM-DD HH:mm"
            onChange={onChange}
            {...styles}
        />
    );
};

DateTimePickerRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
