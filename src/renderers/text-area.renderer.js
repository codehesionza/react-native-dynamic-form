import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { Field } from '../types/types';
import Default from '../components/inputs/default.component';

export const TextAreaRenderer = (props) => {
    const { field, onChange, styles = {}, numberOfLines = 5 } = props;

    return (
        <Default
            field={field}
            onChange={onChange}
            multiline
            numberOfLines={numberOfLines}
            {...styles}
        />
    );
};

TextAreaRenderer.propTypes = {
    field: PropTypes.shape(Field),
    numberOfLines: PropTypes.number,
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
