import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { Field } from '../types/types';
import Pin from '../components/inputs/pin.component';

export const PinRenderer = (props) => {
    const { field, onChange, styles = {} } = props;

    return <Pin field={field} onChange={onChange} {...styles} />;
};

PinRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
