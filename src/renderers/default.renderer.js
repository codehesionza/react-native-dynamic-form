import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { Field } from '../types/types';
import Default from '../components/inputs/default.component';

export const DefaultRenderer = (props) => {
    const { field, onChange, styles = {} } = props;

    return <Default field={field} onChange={onChange} {...styles} />;
};

DefaultRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
