import PropTypes from 'prop-types';
import { ViewPropTypes } from 'react-native';
import React from 'react';

import { Field } from '../types/Field';
import Secure from '../components/inputs/secure.component';

export const PasswordRenderer = (props) => {
    const { field, onChange, styles = {} } = props;

    return <Secure field={field} onChange={onChange} {...styles} />;
};

PasswordRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    onChange: PropTypes.func
};
