import React from 'react';
import { ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import Picker from '../components/inputs/picker.component';
import { Field } from '../types/Field';

export const PickerRenderer = (props) => {
    const { field, getFieldAttribute, onChange, styles = {} } = props;

    return (
        <Picker
            field={field}
            getFieldAttribute={getFieldAttribute}
            onChange={onChange}
            includeEmptyItem={false}
            {...styles}
        />
    );
};

PickerRenderer.propTypes = {
    field: PropTypes.shape(Field),
    styles: PropTypes.arrayOf(ViewPropTypes.style),

    getFieldAttribute: PropTypes.func,
    onChange: PropTypes.func
};
